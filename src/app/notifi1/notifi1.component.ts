import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../service/notification.service';
import { FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-notifi1',
  templateUrl: './notifi1.component.html',
  styleUrls: ['./notifi1.component.css']
})
export class Notifi1Component implements OnInit {
  constructor(private notifier: NotificationService, private _form: FormBuilder) { }
get Input (){
  return this.RegistrationForm.get('Input');
}
get checkBox(){
  return this.RegistrationForm.get('checkBox');
}
  RegistrationForm = this._form.group({
    checkBox: [false],
    Input: ['']

  })

  currentInputMessage = {msg: null, checkBox: null, sender: null}
 
  ngOnInit(): void {
    this.notifier.notifier.subscribe((x) => {
      this.currentInputMessage = x;
    });
  }

  sendMessage() {

    this.notifier.sendNotification({msg: this.Input.value, checkBox: this.checkBox.value, sender: 'person 1'});

  }
}

