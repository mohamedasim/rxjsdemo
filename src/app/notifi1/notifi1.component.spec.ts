import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Notifi1Component } from './notifi1.component';

describe('Notifi1Component', () => {
  let component: Notifi1Component;
  let fixture: ComponentFixture<Notifi1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Notifi1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Notifi1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
