import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../service/notification.service';
@Component({
  selector: 'app-notifi3',
  templateUrl: './notifi3.component.html',
  styleUrls: ['./notifi3.component.css']
})
export class Notifi3Component implements OnInit {
  msgs = [];
  constructor(private notifier: NotificationService) { }


  currentInputMessage=[{msg:null,checkBox:null,sender:null}];
  ngOnInit(): void {
    
    this.notifier.notifier.subscribe(x=>{
      if(x.checkBox){
        this.currentInputMessage.push(x)
    }
  })
}
}