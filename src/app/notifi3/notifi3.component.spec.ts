import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Notifi3Component } from './notifi3.component';

describe('Notifi3Component', () => {
  let component: Notifi3Component;
  let fixture: ComponentFixture<Notifi3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Notifi3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Notifi3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
