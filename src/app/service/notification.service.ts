import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class NotificationService {

  notifier:BehaviorSubject<any> = new BehaviorSubject<any>(null);
  constructor() {

  }

  sendNotification(value: any) {
    debugger
      this.notifier.next(value);
     
    }
}
