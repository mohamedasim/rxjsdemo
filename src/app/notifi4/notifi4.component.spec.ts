import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Notifi4Component } from './notifi4.component';

describe('Notifi4Component', () => {
  let component: Notifi4Component;
  let fixture: ComponentFixture<Notifi4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Notifi4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Notifi4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
