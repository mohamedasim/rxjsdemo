import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Notifi2Component } from './notifi2.component';

describe('Notifi2Component', () => {
  let component: Notifi2Component;
  let fixture: ComponentFixture<Notifi2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Notifi2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Notifi2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
