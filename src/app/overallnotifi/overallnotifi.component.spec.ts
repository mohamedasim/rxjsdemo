import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverallnotifiComponent } from './overallnotifi.component';

describe('OverallnotifiComponent', () => {
  let component: OverallnotifiComponent;
  let fixture: ComponentFixture<OverallnotifiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverallnotifiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverallnotifiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
