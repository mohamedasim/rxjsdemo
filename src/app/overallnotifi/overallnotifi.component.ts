import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../service/notification.service';
@Component({
  selector: 'app-overallnotifi',
  templateUrl: './overallnotifi.component.html',
  styleUrls: ['./overallnotifi.component.css']
})
export class OverallnotifiComponent implements OnInit {

  constructor(private notifier: NotificationService) { }

  currentInputMessage = [{msg: null, checkBox: null, sender: null}];
  ngOnInit(): void {
    this.notifier.notifier.subscribe(x => {this.currentInputMessage.push(x)} );
 }
}

