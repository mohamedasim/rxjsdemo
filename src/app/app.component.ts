import { Component } from '@angular/core';
import { NotificationService } from './service/notification.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'rxjsdemo.';
  constructor(private notifier: NotificationService) {
    notifier.sendNotification("this is asim");
    let num = 10;
    // setInterval((x) => {
    //   num = num + 1;
    //   notifier.sendNotification("this is asim" + num);
    // }, 1000);
  }

}
