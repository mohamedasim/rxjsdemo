import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Notifi1Component } from './notifi1/notifi1.component';
import { Notifi2Component } from './notifi2/notifi2.component';
import { OverallnotifiComponent } from './overallnotifi/overallnotifi.component';
import { Notifi3Component } from './notifi3/notifi3.component';
import { Notifi4Component } from './notifi4/notifi4.component';

@NgModule({
  declarations: [
    AppComponent,
    Notifi1Component,
    Notifi2Component,
    OverallnotifiComponent,
    Notifi3Component,
    Notifi4Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
